var sortearBtn = document.getElementById('sortearBtn');
var numeroSorteadoParagrafo = document.getElementById('numeroSorteado');
var slider = document.getElementById('slider');
var minValueSpan = document.getElementById('minValue');
var maxValueSpan = document.getElementById('maxValue');

noUiSlider.create(slider, {
    start: [1, 100],
    connect: true,
    range: {
        'min': 1,
        'max': 100
    }
});

sortearBtn.addEventListener('click', function () {
    var valoresSlider = slider.noUiSlider.get();
    var minimo = parseInt(valoresSlider[0]);
    var maximo = parseInt(valoresSlider[1]);

    var numeroSorteado = Math.floor(Math.random() * (maximo - minimo + 1)) + minimo;
    numeroSorteadoParagrafo.textContent = 'O número sorteado é: ' + numeroSorteado;
});

slider.noUiSlider.on('update', function (values, _handle) {
    var minimo = parseInt(values[0]);
    var maximo = parseInt(values[1]);
    minValueSpan.textContent = 'Mínimo: ' + minimo;
    maxValueSpan.textContent = 'Máximo: ' + maximo;
});
