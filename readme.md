# Sorteador de Números

O **Sorteador de Números** é uma aplicação web simples e intuitiva que permite aos usuários sortear números aleatórios dentro de um intervalo definido. 

## Recursos

- **Intervalo Personalizável:** Defina o intervalo de números desejado utilizando a barra deslizante.

## Como Usar

1. Deslize o controle deslizante para definir o intervalo de números que você deseja sortear.
2. Clique no botão "Sortear Número" para gerar um número aleatório dentro do intervalo definido.
3. O número sorteado será exibido na tela.

Divirta-se e boa sorte! 🎉
